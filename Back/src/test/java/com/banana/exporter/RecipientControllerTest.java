package com.banana.exporter;

import com.banana.exporter.entities.Recipient;
import io.quarkus.test.junit.QuarkusTest;
import org.junit.jupiter.api.Test;

import javax.ws.rs.core.Response;

import static io.restassured.RestAssured.given;
import static org.hamcrest.CoreMatchers.is;

@QuarkusTest
class RecipientControllerTest {

    @Test
    void workflowTest() {
        given()
                .when().get("/recipients")
                .then()
                .statusCode(Response.Status.OK.getStatusCode()).body(is("[]"));

        Recipient r = new Recipient();
        r.name = "name";
        r.address = "address";
        r.city = "city";
        r.country = "country";
        r.postalCode = "postalCode";

        given().body(r).contentType("application/json").when().post("/recipients")
                .then()
                .statusCode(Response.Status.CREATED.getStatusCode());

        given()
                .when().get("/recipients")
                .then()
                .statusCode(Response.Status.OK.getStatusCode()).body("size()", is(1));

        given()
                .when().get("/recipients/1")
                .then()
                .statusCode(Response.Status.OK.getStatusCode())
                .body("id", is(1))
                .body("name", is(r.name))
                .body("address", is(r.address))
                .body("city", is(r.city))
                .body("country", is(r.country))
                .body("postalCode", is(r.postalCode));

        r.name = "newName";

        given().body(r).contentType("application/json").when().put("/recipients/1")
                .then()
                .statusCode(Response.Status.OK.getStatusCode()).body("name", is("newName"));

        given()
                .when().get("/recipients/1")
                .then()
                .statusCode(Response.Status.OK.getStatusCode())
                .body("id", is(1))
                .body("name", is(r.name))
                .body("address", is(r.address))
                .body("city", is(r.city))
                .body("country", is(r.country))
                .body("postalCode", is(r.postalCode));


        given().when().delete("/recipients/1")
                .then()
                .statusCode(Response.Status.NO_CONTENT.getStatusCode());

        given()
                .when().get("/recipients")
                .then()
                .statusCode(Response.Status.OK.getStatusCode()).body(is("[]"));
    }

}