package com.banana.exporter.entities;

import com.banana.exporter.exceptions.UnknownOrderException;
import com.banana.exporter.validators.OneWeekAfter;
import io.quarkus.hibernate.orm.panache.PanacheEntity;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;

@Entity
@Table(name = "orderTable")
public class Order extends PanacheEntity {
    @NotNull(message = "Recipient may not be null")
    @ManyToOne
    public Recipient recipient;

    @NotNull(message = "Delivery may not be null")
    @OneWeekAfter
    public LocalDate deliveryDate;

    public int weight;
    public Double price;

    public static Order getOrderByIdOrThrowMissing(Long id) {
        Order entity = Order.findById(id);
        if (entity == null) {
            Order unknown = new Order();
            unknown.id = id;
            throw new UnknownOrderException(unknown);
        }
        return entity;
    }

    @Override
    public String toString() {
        return "Order{" +
                "recipient=" + recipient +
                ", deliveryDate=" + deliveryDate +
                ", weight=" + weight +
                ", price=" + price +
                ", id=" + id +
                '}';
    }
}
