package com.banana.exporter.entities;

import com.banana.exporter.exceptions.UnknownRecipientException;
import io.quarkus.hibernate.orm.panache.PanacheEntity;
import io.quarkus.panache.common.Parameters;

import javax.persistence.Entity;
import javax.validation.constraints.NotBlank;

@Entity
public class Recipient extends PanacheEntity {
    @NotBlank(message = "name may not be blank")
    public String name;

    @NotBlank(message = "address may not be blank")
    public String address;

    @NotBlank(message = "postalCode may not be blank")
    public String postalCode;

    @NotBlank(message = "city may not be blank")
    public String city;

    @NotBlank(message = "country may not be blank")
    public String country;

    public static Recipient findDuplicate(Recipient recipient) {
        return find("name = :name and address = :address and postalCode = :postalCode and city = :city and country = :country",
                Parameters.with("name", recipient.name)
                        .and("address", recipient.address)
                        .and("postalCode", recipient.postalCode)
                        .and("city", recipient.city)
                        .and("country", recipient.country)
                        .map())
                .firstResult();
    }

    public static Recipient getRecipientByIdOrThrowMissing(Long id) {
        Recipient recipient = Recipient.findById(id);
        if (recipient == null) {
            Recipient unknown = new Recipient();
            unknown.id = id;
            throw new UnknownRecipientException(unknown);
        }
        return recipient;
    }

    @Override
    public String toString() {
        return "Recipient{" +
                "name='" + name + '\'' +
                ", address='" + address + '\'' +
                ", postalCode='" + postalCode + '\'' +
                ", city='" + city + '\'' +
                ", country='" + country + '\'' +
                ", id=" + id +
                '}';
    }

}
