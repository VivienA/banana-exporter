package com.banana.exporter;

import com.banana.exporter.entities.Recipient;
import com.banana.exporter.exceptions.DuplicateRecipientException;
import com.banana.exporter.exceptions.UnknownRecipientException;
import io.smallrye.common.annotation.Blocking;

import javax.transaction.Transactional;
import javax.validation.Valid;
import javax.ws.rs.*;
import javax.ws.rs.core.Response;
import java.net.URI;
import java.util.List;

@Path("/recipients")
@Blocking
public class RecipientController {

    @GET
    public List<Recipient> list() {
        return Recipient.listAll();
    }

    @GET
    @Path("/{id}")
    public Recipient get(@PathParam("id") Long id) {
        return Recipient.getRecipientByIdOrThrowMissing(id);
    }

    @POST
    @Transactional
    public Response create(@Valid Recipient recipient) {
        Recipient duplicate = Recipient.findDuplicate(recipient);
        if (duplicate != null) {
            throw new DuplicateRecipientException(duplicate);
        }
        recipient.persist();
        return Response.created(URI.create("/persons/" + recipient.id)).build();
    }

    @PUT
    @Path("/{id}")
    @Transactional
    public Response update(@PathParam("id") Long id, @Valid Recipient recipient) {
        Recipient entity = Recipient.findById(id);
        if (entity == null) {
            throw new UnknownRecipientException(recipient);
        }

        entity.name = recipient.name;
        entity.address = recipient.address;
        entity.postalCode = recipient.postalCode;
        entity.city = recipient.city;
        entity.country = recipient.country;

        return Response.status(Response.Status.OK).entity(entity).build();
    }

    @DELETE
    @Path("/{id}")
    @Transactional
    public Response delete(@PathParam("id") Long id) {
        Recipient entity = Recipient.getRecipientByIdOrThrowMissing(id);
        entity.delete();

        return Response.noContent().build();
    }


}
