package com.banana.exporter;

import com.banana.exporter.entities.Order;
import com.banana.exporter.entities.Recipient;
import com.banana.exporter.models.OrderDTO;
import io.smallrye.common.annotation.Blocking;

import javax.transaction.Transactional;
import javax.validation.Valid;
import javax.ws.rs.*;
import javax.ws.rs.core.Response;
import java.net.URI;
import java.util.List;

@Path("/orders")
@Blocking
public class OrderController {
    @GET
    public List<OrderDTO> listAll(@PathParam("id") Long id) {
        return Order.findAll().project(OrderDTO.class).list();
    }

    @GET
    @Path("/recipients/{id}")
    public List<OrderDTO> listForRecipient(@PathParam("id") Long id) {
        return Order.find("recipient.id", id).project(OrderDTO.class).list();
    }

    @GET
    @Path("/{id}")
    public Order get(@PathParam("id") Long id) {
        return Order.getOrderByIdOrThrowMissing(id);
    }

    @POST
    @Transactional
    public Response create(@Valid OrderDTO orderDto) {
        Recipient recipient = Recipient.getRecipientByIdOrThrowMissing(orderDto.recipientId);

        Order order = new Order();
        order.recipient = recipient;
        order.deliveryDate = orderDto.deliveryDate;
        order.weight = orderDto.getWeight();
        order.price = orderDto.getPrice();

        order.persist();

        return Response.created(URI.create("/orders/" + order.id)).build();
    }

    @PUT
    @Path("/{id}")
    @Transactional
    public Response update(@PathParam("id") Long id, @Valid OrderDTO orderDto) {
        Order entity = Order.getOrderByIdOrThrowMissing(id);

        entity.recipient = Recipient.getRecipientByIdOrThrowMissing(orderDto.recipientId);
        entity.weight = orderDto.getWeight();
        entity.price = orderDto.getPrice();

        return Response.status(Response.Status.OK).entity(entity).build();
    }

    @DELETE
    @Path("/{id}")
    @Transactional
    public Response delete(@PathParam("id") Long id) {
        Order entity = Order.getOrderByIdOrThrowMissing(id);
        entity.delete();

        return Response.noContent().build();
    }

}