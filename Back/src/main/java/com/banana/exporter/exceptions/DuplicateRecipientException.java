package com.banana.exporter.exceptions;

import com.banana.exporter.entities.Recipient;

public class DuplicateRecipientException extends RuntimeException {
    public final Recipient recipient;

    public DuplicateRecipientException(Recipient recipient) {
        this.recipient = recipient;
    }
}
