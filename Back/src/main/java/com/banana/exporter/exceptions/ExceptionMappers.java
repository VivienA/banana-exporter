package com.banana.exporter.exceptions;

import com.banana.exporter.models.RecipientRequestError;
import org.jboss.resteasy.reactive.RestResponse;
import org.jboss.resteasy.reactive.server.ServerExceptionMapper;

import javax.ws.rs.core.Response;

class ExceptionMappers {
    @ServerExceptionMapper
    public RestResponse<RecipientRequestError> mapDuplicateException(DuplicateRecipientException x) {
        return getEntityError(x.recipient != null ? x.recipient.toString() : "null recipient", "Recipient already exist", Response.Status.CONFLICT);
    }

    @ServerExceptionMapper
    public RestResponse<RecipientRequestError> mapUnknownException(UnknownRecipientException x) {
        return getEntityError(x.recipient != null ? x.recipient.toString() : "null recipient", "Missing recipient", Response.Status.NOT_FOUND);
    }

    @ServerExceptionMapper
    public RestResponse<RecipientRequestError> mapUnknownOrderException(UnknownOrderException x) {
        return getEntityError(x.order != null ? x.order.toString() : "null recipient", "Missing recipient", Response.Status.NOT_FOUND);
    }

    private RestResponse<RecipientRequestError> getEntityError(String data, String error, Response.Status code) {
        return RestResponse.status(code, new RecipientRequestError(data, error, code));
    }
}