package com.banana.exporter.exceptions;

import com.banana.exporter.entities.Recipient;

public class UnknownRecipientException extends RuntimeException {
    public final Recipient recipient;

    public UnknownRecipientException(Recipient recipient) {
        this.recipient = recipient;
    }
}
