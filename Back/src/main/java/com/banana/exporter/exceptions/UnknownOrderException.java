package com.banana.exporter.exceptions;

import com.banana.exporter.entities.Order;

public class UnknownOrderException extends RuntimeException {
    public final Order order;

    public UnknownOrderException(Order order) {
        this.order = order;
    }
}
