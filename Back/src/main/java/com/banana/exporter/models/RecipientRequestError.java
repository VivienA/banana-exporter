package com.banana.exporter.models;

import javax.ws.rs.core.Response;

public class RecipientRequestError {
    private final String data;
    private final String error;
    private final String reason;
    private final int code;

    public RecipientRequestError(String data, String error, Response.Status code) {
        this.data = data;
        this.error = error;
        this.code = code.getStatusCode();
        this.reason = code.getReasonPhrase();
    }


    public String getError() {
        return error;
    }

    public String getReason() {
        return reason;
    }

    public int getCode() {
        return code;
    }

    @Override
    public String toString() {
        return "RecipientRequestError{" +
                "data=" + data +
                ", error='" + error + '\'' +
                ", reason='" + reason + '\'' +
                ", code=" + code +
                '}';
    }
}
