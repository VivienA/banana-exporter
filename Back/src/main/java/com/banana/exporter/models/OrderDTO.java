package com.banana.exporter.models;

import com.banana.exporter.validators.OneWeekAfter;
import io.quarkus.hibernate.orm.panache.common.ProjectedFieldName;
import io.quarkus.runtime.annotations.RegisterForReflection;
import org.eclipse.microprofile.config.ConfigProvider;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;

@RegisterForReflection
public class OrderDTO {
    public Long id;

    Integer weightPerCrate = ConfigProvider.getConfig().getValue("app.crate.weight", Integer.class);
    Double pricePerKg = ConfigProvider.getConfig().getValue("app.kg.price", Double.class);

    @NotNull(message = "recipient may not be null")
    public Long recipientId;

    @NotNull(message = "deliveryDate may not be null")
    @OneWeekAfter
    public LocalDate deliveryDate;

    @Min(value = 0, message = "Number of crates must be positive or 0")
    @Max(value = 10_000, message = "maximum 10 000 crates can be ordered")
    @NotNull(message = "deliveryDate may not be null")
    public Integer crates;

    public Double getPrice() {
        return getWeight() * pricePerKg;
    }

    public int getWeight() {
        return crates * weightPerCrate;
    }

    public OrderDTO(@ProjectedFieldName("recipient.id") Long recipientId, LocalDate deliveryDate, int weight, Long id) {
        this.recipientId = recipientId;
        this.deliveryDate = deliveryDate;
        this.crates = weight / weightPerCrate;
        this.id = id;
    }
}
