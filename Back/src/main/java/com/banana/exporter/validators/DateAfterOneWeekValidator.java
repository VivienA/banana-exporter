package com.banana.exporter.validators;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.time.LocalDate;

public class DateAfterOneWeekValidator implements ConstraintValidator<OneWeekAfter, LocalDate> {

    public final void initialize(final OneWeekAfter annotation) {
    }

    public final boolean isValid(final LocalDate value, final ConstraintValidatorContext context) {
        LocalDate inOneWeek = LocalDate.now().plusWeeks(1);
        return value.isAfter(inOneWeek);
    }
}