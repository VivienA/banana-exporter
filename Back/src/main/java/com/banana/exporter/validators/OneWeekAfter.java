package com.banana.exporter.validators;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

@Documented
@Constraint(validatedBy = DateAfterOneWeekValidator.class)
@Target( { ElementType.METHOD, ElementType.FIELD })
@Retention(RetentionPolicy.RUNTIME)
public @interface OneWeekAfter {
    String message() default "Date is before next week";
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};
}